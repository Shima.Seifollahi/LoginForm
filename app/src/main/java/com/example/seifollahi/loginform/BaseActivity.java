package com.example.seifollahi.loginform;

import android.app.Activity;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

/**
 * Created by seifollahi on 10/13/2017.
 */

public class BaseActivity extends AppCompatActivity {
    Context mContext = this;
    Activity mActivity = this;

    public  void showToast (String txt)
    {
        Toast.makeText(mContext,txt, Toast.LENGTH_SHORT).show();


    }
}


