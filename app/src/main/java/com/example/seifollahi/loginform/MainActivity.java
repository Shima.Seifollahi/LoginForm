package com.example.seifollahi.loginform;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends BaseActivity implements View.OnClickListener {

    TextView shTitle;
    EditText fName;
    EditText lName;
    EditText phoneNum;
    Button satartSecondActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bindViews();
    }

    private void bindViews() {
        satartSecondActivity = (Button) findViewById(R.id.satartSecondActivity);
        satartSecondActivity.setOnClickListener(this);
        shTitle = (TextView) findViewById(R.id.title);
        fName = (EditText) findViewById(R.id.fName);
        lName = (EditText) findViewById(R.id.lName);
        phoneNum = (EditText) findViewById(R.id.phoneNum);

        findViewById(R.id.showString).setOnClickListener(new TextView.OnClickListener() {
                                                             @Override
                                                             public void onClick(View view) {
                                                                 shTitle.setText(fName.getText() + " " + lName.getText() + " " + phoneNum.getText());
                                                                 showToast(shTitle.toString());
                                                             }
                                                         }


        );

    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.satartSecondActivity){
            Intent secondActivityIntent = new Intent(mContext,SecondActivity.class);
            secondActivityIntent.putExtra("name","Shima");
            secondActivityIntent.putExtra("family","Seifollahi");
            secondActivityIntent.putExtra("age",33);
            startActivity(secondActivityIntent);

        }



    }
}
