package com.example.seifollahi.loginform;

import android.content.Context;
import android.preference.PreferenceManager;

/**
 * Created by seifollahi on 10/13/2017.
 */

public class Publics {
    public static void setShared(Context mContext, String Key, String Value) {
        PreferenceManager.getDefaultSharedPreferences(mContext).edit().putString(Key, Value).apply();
    }

    public static String getShared(Context mContext,String Key, String def_Value) {
        return PreferenceManager.getDefaultSharedPreferences(mContext).getString(Key, def_Value);
    }
}
