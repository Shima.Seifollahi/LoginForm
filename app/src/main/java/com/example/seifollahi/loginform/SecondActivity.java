package com.example.seifollahi.loginform;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.TextView;

/**
 * Created by seifollahi on 10/13/2017.
 */

public class SecondActivity extends BaseActivity {
    TextView result;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        bindViews();

        String res = getIntent().getStringExtra("name") + " " +
                getIntent().getStringExtra("family") + "age is : " +
                getIntent().getIntExtra("age", 49);


        result.setText(res);
    }

    private void bindViews() {
        result = (TextView) findViewById(R.id.result);
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        showToast("Pleas Waite!!!!!!!");
    }
}
