package com.example.seifollahi.loginform;

import android.content.Intent;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class SharedActivity extends BaseActivity implements View.OnClickListener {

    TextView result;
    EditText name;
    EditText family;
    ImageView image;
//    EditText number;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shared);
        bindViews();
    }

    private void bindViews() {

        result = (TextView) findViewById(R.id.result);
        name = (EditText) findViewById(R.id.name);
        family = (EditText) findViewById(R.id.family);
        image =(ImageView) findViewById(R.id.image);

        //Picasso.with(mContext).load("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR9PNXSoqwX4y9BUA1ZFTlgp4uQZYfsOgh07geaoAcXfLY-jI0S").into(image);
        //Picasso.with(mContext).load("C:\\Users\\seifollahi\\Desktop\\imagesZ75L29TA").into(image);
//        number = (EditText) findViewById(R.id.number);
        findViewById(R.id.show).setOnClickListener(this);
        findViewById(R.id.save).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.show) {
            String str;
//            str = getShared("name", "noname") + " " +
//                    getShared("family", "nofamily");
            str = Publics.getShared(mContext, "name", "noname") + " " +
                    Publics.getShared(mContext, "family", "nofamily");
            result.setText(str);


        } else if (view.getId() == R.id.save) {
//            setShared("name", name.getText().toString());
//            setShared("family", family.getText().toString());
            Publics.setShared(mContext, "name", name.getText().toString());
            Publics.setShared(mContext, "family", family.getText().toString());

        }
//        else if (view.getId() == R.id.call) {
//            Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + number.getText().toString()));
//            mContext.startActivity(intent);
//        }

    }

//    void setShared(String Key, String Value) {
//        PreferenceManager.getDefaultSharedPreferences(mContext).edit().putString(Key, Value).apply();
//    }
//
//    String getShared(String Key, String def_Value) {
//        return PreferenceManager.getDefaultSharedPreferences(mContext).getString(Key, def_Value);
//    }
}
