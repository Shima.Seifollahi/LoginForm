package com.example.seifollahi.loginform;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.seifollahi.loginform.adapters.SportListAdapter;

public class ShowListViewActivity extends BaseActivity  {
    ListView showList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_list_view);
        showList = (ListView) findViewById(R.id.showList);
        String sports[]={
                "Football",
                "Volleyball",
                "Basketball",
                "Swimming",
                "Running"

        };

        String sportImages[]={

                         "http://cyprus-mail.com/wp-content/uploads/2016/11/FOOTBALL-1-770x529.jpg",
                         "http://cyprus-mail.com/wp-content/uploads/2016/11/FOOTBALL-1-770x529.jpg",
                         "http://cyprus-mail.com/wp-content/uploads/2016/11/FOOTBALL-1-770x529.jpg",
                         "http://cyprus-mail.com/wp-content/uploads/2016/11/FOOTBALL-1-770x529.jpg",
                         "http://cyprus-mail.com/wp-content/uploads/2016/11/FOOTBALL-1-770x529.jpg"

        };

        SportListAdapter adapter =new SportListAdapter(mContext,sports,sportImages);
        showList.setAdapter(adapter);
        showList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                String clickedItem = (String) adapterView.getItemAtPosition(position);
                showToast(clickedItem);

            }
        });

    }
}
