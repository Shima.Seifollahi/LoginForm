package com.example.seifollahi.loginform.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.seifollahi.loginform.R;
import com.squareup.picasso.Picasso;

/**
 * Created by seifollahi on 10/18/2017.
 */

public class SportListAdapter extends BaseAdapter {
    Context mContext;
    String sports[];
    String sportImages[];

    public SportListAdapter(Context mContext, String[] sports, String[] sportImages) {
        this.mContext = mContext;
        this.sports = sports;
        this.sportImages = sportImages;
    }

    @Override
    public int getCount() {
        return sports.length;
    }

    @Override
    public Object getItem(int position) {
        return sports[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        View rowView = LayoutInflater.from(mContext).inflate(R.layout.sport_list_item, viewGroup, false);
        TextView sportName = (TextView) rowView.findViewById(R.id.sportName);
        ImageView sportImage = (ImageView) rowView.findViewById(R.id.sportImage);
        sportName.setText(sports[position]);
        Picasso.with(mContext).load(sportImages[position]).into(sportImage);
        return rowView;
    }
}
